LTWeb-HUS

Start xampp:
sudo /opt/lampp/lampp start

Stop xampp:
sudo /opt/lampp/lampp stop

Run GUI:
sudo /opt/lampp/./manager-linux-x64.run

cp to htdocs:
sudo cp login.php /opt/lampp/htdocs
sudo cp register.php /opt/lampp/htdocs

These two commands will solve the problem Apache fail on Ubuntu but NOT permanently:
sudo systemctl status apache2
sudo /etc/init.d/apache2 stop
sudo /opt/lampp/lampp start

We can disable the apache2 service so that it won't start everytime we boot our system:
sudo systemctl disable apache2