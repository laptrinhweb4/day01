// Lấy các element input và button
var khoaInput = document.getElementById("input_name")
var tuKhoaInput = document.getElementById("tu_khoa")
var resetButton = document.getElementById('nonsubmitButton');

var searchResultText = document.querySelector('.tittle');
var tableBody = document.querySelector('tbody');

// Thêm sự kiện keyup cho ô "Khoa"
khoaInput.addEventListener('keyup', function() {
    performSearch(); // Gọi hàm thực hiện tìm kiếm
});

// Thêm sự kiện keyup cho ô "Từ khóa"
tuKhoaInput.addEventListener('keyup', function() {
    performSearch(); // Gọi hàm thực hiện tìm kiếm
});

// Đặt sự kiện click cho nút reset
resetButton.addEventListener('click', function() {
    // Xóa giá trị của input
    khoaInput.value = '';
    tuKhoaInput.value = '';

    getAll();
});

function performSearch() {
   // Lấy giá trị từ các ô input
    var khoaValue = khoaInput.value;
    var tuKhoaValue = tuKhoaInput.value;

    // Thực hiện AJAX request
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            // Xử lý kết quả tìm kiếm ở đây
            displaySearchResults(xhr.responseText);
        }
    };

    // Gửi yêu cầu GET đến server với các tham số tìm kiếm
    var url = 'search.php?khoa=' +khoaValue + '&tuKhoa=' + tuKhoaValue;
    console.log(url)
    xhr.open('GET', url, true);
    xhr.send();
}

function getAll() {
     // Thực hiện AJAX request
     var xhr = new XMLHttpRequest();
     xhr.onreadystatechange = function() {
         if (xhr.readyState === 4 && xhr.status === 200) {
             // Xử lý kết quả tìm kiếm ở đây
             displaySearchResults(xhr.responseText);
         }
     };

    // Gửi yêu cầu GET đến server với các tham số tìm kiếm
    var url = 'search.php?khoa=' + '%' + '&tuKhoa=' + '%';
    console.log(url)
    xhr.open('GET', url, true);
    xhr.send();
 
}

function displaySearchResults(responseText) {

    console.log(responseText)

    // Chuyển đổi chuỗi JSON thành đối tượng JavaScript

    var results = JSON.parse(responseText);

    // Xóa nội dung của tbody để cập nhật kết quả mới
    tableBody.innerHTML = '';

    // Hiển thị kết quả trên bảng
    for (var i = 0; i < results.length; i++) {
        var row = results[i];
        var newRow = "<tr><td>" + (i + 1) + "</td><td>" + row.full_name + "</td><td>" + row.faculty + "</td><td><button class='button-container' id='tableButton'>Xóa</button><button class='button-container' id='tableButton'>Sửa</button></td></tr>";
        tableBody.innerHTML += newRow;
    }

    // Cập nhật số sinh viên tìm thấy
    searchResultText.textContent = "Số sinh viên tìm thấy: " + results.length;
}