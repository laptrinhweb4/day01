<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "LTWEBS";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=$database", $username, $password);
        $conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        echo "<p>Connected to server mysql</p><br>";
    } catch (PDOException $e) {
        die("". $e->getMessage());
    }

    return null;
?>