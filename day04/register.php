<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php
        $phanKhoa = array(
            "--Chọn phân khoa--",
            "Khoa học máy tính",
            "Khoa học vật liệu"
        );

        $gioiTinh = array(
            0 => "Nam",
            1 => "Nữ"
        );

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $selectedGioiTinh = isset($_POST["gioiTinh"]) ? $_POST["gioiTinh"] : array();
            $selectedKhoa = $_POST["khoa"];
        }

    ?>


    <div class="container">
    <div id="errorMessage"></div>
        <form>
            <label for="inputname" class="input_name">Họ và tên <span class="required">*</span></label>
            <input type="text" name="inputname" class="entering" required><br><br>
        </form>

        <form>
            <label for="gioitinh" class="input_name input_gen">Giới tính <span class="required">*</span></label>
            <div class="choose_gender">
                <label for="male">
                    <input type="radio" id="male" name="gender"> Nam
                </label>
                <label for="female">
                    <input type="radio" id="female" name="gender"> Nữ
                </label>
            </div>
        </form>

        <form>
            <label for="phankhoa" class="input_name input_falcuty">Phân khoa <span class="required">*</span></label>
            <select name="phankhoa" class="choose-falcuty" required>
                <option value="">-- Chọn phân khoa --</option>
                <option value="KHMT">Khoa học máy tính </option>
                <option value="KHDL">Khoa học dữ liệu </option>
            </select>
        </form>

        <form>
            <label for="ngaysinh" class="input_name date_of_birth">Ngày sinh <span class="required">*</span></label>
            <input type="text" id="dob" name="ngaysinh" class="date_input" placeholder="dd/mm/yyyy" required>
        </form>

        <form>
            <label for="address" class="input_name"> Địa chỉ </label>
            <textarea id="address" name="address" class="input_address" rows="4" cols="30"></textarea>
        </form>
        <button class="button-container" id="submitButton"> Đăng ký </button>
    </div>

    <script type="text/javascript" src="script.js"></script>
</body>
</html>