<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Đăng ký tân sinh viên </title>
    <style>
        .container {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 350px; 
            width: 600px;
            margin: 0; 
            background-color: white; 
            border: 1px solid blue;
            color: blue;
        }
        body {
            display: flex;
            flex-direction: column;
            justify-content: center; 
            align-items: center;
            height: 100vh; 
            margin: 0; 
            background-color: white; 
        }

        .form_name {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 90px; 
            height: 15px;      
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block; 
        }

        .form_gender {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 90px; 
            height: 15px;      
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block;
        }

        .entering{
            border: 1px solid blue;
            color: blue;
            padding: 5px; 
            border-radius: 0px;
            width: 400px;   
            height: 25px;        
            display: inline-block;
            margin: 5px;
            
        }

        .form {
            align-self: flex-start; 
            padding-left: 20px;
        }
        
        .gender_picking{
            padding-left: 10px;
            color: black;
        }
        
        .register_form_falcuty {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 90px; 
            height: 19px;      
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block;
        }
        .falcuty_picking {
            border: 1px solid blue;
            background-color: #34aeeb; 
            color: white; 
            padding: 5px; 
            margin: 5px;
            border-radius: 0px;
            width: 180px;
            height: 40px;     
            padding-top: 9px;
            padding-bottom: 11px;
            text-align: center;     
            display: inline-block;
            padding-left: 10px;
        }

        .button-container {
            border: 1px solid blue;
            background-color: green; 
            color: white; 
            padding: 5px; 
            border-radius: 5px;
            width: 150px;   
            height: 50px;  
            margin-top: 20px; 
            margin-left: 80px;      
        }
    </style>
</head>
<body>
    <?php
        $facultyInfo = array(
            0 => array("gender" => "Male", "faculty" => "Computer Science"),
            1 => array("gender" => "Female", "faculty" => "Materials Science")
        );
    
        if ($_SERVER["REQUEST_METHOD"] === "POST") {

            $selectedDepartment = $_POST["department"];

            if (!empty($selectedDepartment)) {
                echo "<h3>Chọn khoa:</h3>";
                echo "<ul>";
                
                foreach ($facultyInfo as $key => $info) {
                    if ($selectedDepartment === $info["faculty"]) {
                        echo "<li>Gender: " . $info["gender"] . "</li>";
                    }
                }
                echo "</ul>";
            } else {
                echo "<p>Xin hãy chọn phòng ban.</p>";
            }
        }
    ?>

    <div class="container">
        <form class="form" method="POST" action="">
            <label for="account" class = "form_name" >Họ và tên </label>
            <input type="text" id="account" name="account" class = "entering" required><br><br>
        </form>

        <form method="post" class = "form">
            <label for="gender" class = "form_gender"> Giới tính </label>
                <label class = "gender_picking">
                    <input type="radio" name="gender" value="0"> Nam 
                    <input type="radio" name="gender" value="1"> Nữ 
                </label>
            <br>
            <br>
        </form>

        <form method="post" class = "form">
            <label for="department" class = "register_form_falcuty">Phân khoa </label>
            <select name="department" id="department" class="falcuty_picking">
                <option value="">--Chọn phân khoa --</option>
                <option value="MAT">Khoa học máy tính</option>
                <option value="KDL">Khoa học dữ liệu</option>
            </select>
            <br>
            <br>
        </form>
        
        <button class = "button-container"> Đăng ký </button>
    </div>
</body>
</html>