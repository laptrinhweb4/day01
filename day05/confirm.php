<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận đăng ký</title>
    <link rel="stylesheet" href="confirm_style.css">
</head>
<body>
    <div class="container"> 
        <div id = "errorMessage"></div>

        <form action="">
            <label for="inputname" class="input_name">Họ và tên</label>
            <p id="name"></p>
        </form>

        <form action="">
            <label for="gioitinh" class="input_name input_gen">Giới tính</label>
            <p id="gender"></p>
        </form>

        <form action="">
            <label for="phankhoa" class="input_name input_falcuty">Phân khoa</label>
            <p id="khoa"></p>
        </form>

        <form action="">
            <label for="ngaysinh" class="input_name date_of_birth">Ngày sinh </label>
            <p id="dob"></p>
        </form>

        <form action="">
            <label for="address" class="input_name address"> Địa chỉ </label>
            <p id="address"></p>
        </form>

        <form action="">
            <label for="image" class="input_name uploadImage"> Hình ảnh </label>
            <img id="image" src="" alt="Ảnh" >
        </form>

        <button class="button-container" id="confirmButton"> Xác nhận</button>
    </div>

    <script type="text/javascript" src="confirm_script.js"></script>
</body>
</html>