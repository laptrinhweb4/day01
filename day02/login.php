<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        #login-form {
            text-align: center;
            border-style: groove;
            width: 500px;
            margin: auto;
        }

        #container {
            margin: 5px 5px 5px 5px;
        }

        #time-box {
            background-color: #e8e7e6;
            text-align: center;
            border-style: none;
            margin-left: 65px;
            margin-right: 65px
        }

        #button-id {
            border-radius: 5px;
            background-color: #25ace6;
            color: white;
            margin-top: 5px;
        }

        #password-label {
            padding-left: 21.5px;
            padding-right: 21.5px;
            margin: 15px;
            background-color: #25ace6;
            border: 2px solid blue;
            color: white;
            width: 100%;
            text-align: center;
        }

        #username-label {
            background-color: #25ace6;
            border: 2px solid blue;
            color: white;
            width: 100%;
            margin: 15px;
            text-align: center;
        }

        input {
            border: 2px solid blue;
        }

    </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 

        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $daysOfWeek = [
            1 => "thứ 2",
            2 => "thứ 3",
            3 => "thứ 4",
            4 => "thứ 5",
            5 => "thứ 6",
            6 => "thứ 7",
            7 => "Chủ nhật"
        ];

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
             // Lấy giá trị từ form đăng nhập
            $username = $_POST["username"];
            $password = $_POST["password"];

            if ($username == "admin" && $password == "admin123") {
                echo "Đăng nhập thành công!";
              } else {
                echo "Đăng nhập thất bại!";
              }
        }
    ?>

    <div id="login-form">
        <div id="container">
            <p id="time-box" ><?php echo "Bây giờ là: " . date("H:i:s") . ", " . $daysOfWeek[date("N")] . " ngày " .date("d/m/Y"); ?></p>

            <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
                <label id="username-label" for="username">Tên đăng nhập</label> 
                <input type="text" id="username" name="username" required><br>
                <br>

                <label id="password-label" for="password">Mật khẩu</label>
                <input type="password" id="password" name="password" required><br>

                <br>

                <input id="button-id" type="submit" value="Đăng nhập">
            </form>
        </div>
    </div>
    
</body>
</html>