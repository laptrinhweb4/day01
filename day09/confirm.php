<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận</title>
    <link rel="stylesheet" href="./styles/confirm_style.css">
</head>
<body>
    <div class="container">
        <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $name = $_POST["inputname"];
                $gender = $_POST["gender"];
                $major = $_POST["phankhoa"];
                $dob = $_POST["ngaysinh"];
                $address = $_POST["address"];

                echo '<p>Họ tên: ' . $name . '</p>';
                echo '<p>Giới tính: ' . $gender . '</p>'; 
                echo '<p>Phân khoa: ' . $major . '</p>';
                echo '<p> Ngày sinh: '. $dob . '</p>';
                echo '<p> Địa chỉ: '. $address . '</p>';
                
                
                //Handle upload file
                $target_dir    = __DIR__.DIRECTORY_SEPARATOR;
                $target_file   = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                $uploadOk = 1;
                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

                // Check if image file is a actual image or fake image
                if(isset($_POST["submit"])) {
                    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
                    if($check !== false) {
                        echo "File is an image - " . $check["mime"] . ".";
                        $uploadOk = 1;
                    } else {
                        echo "File is not an image.";
                        $uploadOk = 0;
                    }
                }
                
                //Check if file already exists
                if (file_exists($target_file)) {
                    echo "Sorry, file already exists.";
                    $uploadOk = 0;
                }

                // Check file size
                if ($_FILES["fileToUpload"]["size"] > 500000000) {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }
                
                // Allow certain file formats
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {
                    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOk = 0;
                }

                // Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    echo "Sorry, your file was not uploaded.";
                } else {
                    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                        echo "<img src='$target_file' alt='Uploaded Image'>"; 
                    } else {
                        echo "Error when move uploaded file: " . $target_file;
                    }
                    
                }

                //

                
            } else {
                echo "No POST!";
            }
        ?>

        <form action="">
            <button class="button-container" id="confirmButton"> Xác nhận</button>
        </form>
        <?php 
                require_once "database.php";
                
                $date = explode('/' , $dob );
                $dateOfBirth = $date[2]."-".$date[1]."-".$date[0];

                $sql = "INSERT INTO STUDENTS (full_name, gender, faculty, date_of_birth, address, image_path) 
                            VALUES ('$name', '$gender', '$major', '$dateOfBirth', '$address', '$target_file')";

                if ($conn->query($sql)) {
                    echo "<p>Saved</p>";
                } else {
                    echo "<p>Lỗi</p>";
                }
        ?>

    </div>
</body>
</html>