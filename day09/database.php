<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $database = "LTWEBS";

    try {
        $conn = new  mysqli($servername, $username, $password, $database);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } else {
            echo("Connected to database");
        }
    } catch (PDOException $e) {
        die("". $e->getMessage());
    }

    return null;
?>