<?php 
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "LTWEBS";

    $conn = new mysqli($servername, $username, $password, $dbname);

    // Kiểm tra kết nối
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Lấy dữ liệu từ các tham số truyền vào
    $khoa = $_GET['khoa'];
    $tuKhoa = $_GET['tuKhoa'];

    // Sử dụng prepared statement để tránh SQL injection
    $sql = "SELECT full_name, faculty FROM STUDENTS WHERE faculty LIKE ? AND full_name LIKE ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ss", $khoa, $tuKhoa);

    // Thực hiện truy vấn
    $stmt->execute();
    $result = $stmt->get_result();

    // Tạo mảng chứa kết quả
    $rows = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }

    // Trả kết quả dưới dạng JSON
    header('Content-Type: application/json');
    echo json_encode($rows);

    // Đóng kết nối
    $stmt->close();
    $conn->close();
?>