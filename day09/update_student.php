<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Kết nối đến cơ sở dữ liệu
include 'database.php';

// Kiểm tra xem có ID sinh viên được truyền từ trang danh sách sinh viên không
if (isset($_GET['id']) && !empty($_GET['id'])) {
    // Lấy ID sinh viên từ tham số truyền vào
    $studentId = $_GET['id'];

    // Chuẩn bị câu truy vấn để lấy thông tin sinh viên từ cơ sở dữ liệu
    $sql = "SELECT * FROM STUDENTS WHERE id = $studentId";
    $result = $conn->query($sql);

    // Kiểm tra xem có dữ liệu sinh viên hay không
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();

        // Lấy thông tin sinh viên từ cơ sở dữ liệu
        $fullName = $row['full_name'];
        $gender = $row['gender'];
        $faculty = $row['faculty'];
        $dob = $row['date_of_birth'];
        $address = $row['address'];
        $image_path = $row['image_path'];

        // Hiển thị form nhập liệu với thông tin sinh viên được lấy từ cơ sở dữ liệu
        ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Cập nhật thông tin sinh viên</title>
            <link rel="stylesheet" href="./styles/register_style.css">
        </head>
        <body>
            <!--  -->
            <div class="container">
                <form action="update_process.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="student_id" value="<?php echo $studentId; ?>">

                    <div class="form-element">
                        <label for="inputname" class="input_name"> Họ và tên <span class="required">*</span></label>
                        <input type="text" name="inputname" class="entering" value="<?php echo $fullName; ?>" required>
                    </div>

            <!--  -->
            <div class="form-element">
                <label for="gioitinh" class="input_name input_gen">Giới tính <span class="required">*</span></label>
                <div class="choose_gender">
                    <label for="male">
                        <input type="radio" id="male" name="gender" value="Nam"> Nam
                    </label>
                    <label for="female">
                        <input type="radio" id="female" name="gender" value="Nữ"> Nữ
                    </label>
                </div>
            </div>

            <!--  -->
            <div class="form-element">
                <label for="phankhoa" class="input_name input_falcuty">Phân khoa <span class="required">*</span></label>
                <select name="phankhoa" class="choose-falcuty" value="<?php echo $faculty; ?>" required>
                    <option value="">-- Chọn phân khoa --</option>
                    <option value="KHMT">Khoa học máy tính </option>
                    <option value="KHDL">Khoa học dữ liệu </option>
                </select> 
            </div>

            <!--  -->
            <div class="form-element">
                <label for="ngaysinh" class="input_name date_of_birth"> Ngày sinh <span class="required">*</span></label>
                <input type="text" id="dob" name="ngaysinh" class="date_input" placeholder="dd/mm/yyyy" value="<?php echo $dob; ?>" required>
            </div>

            <!--  -->
            <div class="form-element">
                <label for="address" class="input_name address"> Địa chỉ </label>
                <textarea id="address" name="address" class="input_address" rows="4" cols="30" value="<?php echo $address; ?>"></textarea>
            </div>
            
            <!--  -->
            <div class="form-element">
                <label for="image" class="input_name uploadImage" > Hình ảnh </label>
                <input id="image" type="file" name="fileToUpload" class="fileToUpload" value="<?php echo $image_path; ?>">
            </div>

                    <div class="button-submit">
                        <button type="submit" class="button-container" id="submitButton"> Cập nhật </button>
                    </div>
                </form>
            </div>
        </body>
        </html>
        <?php
    } else {
        // Nếu không có dữ liệu sinh viên, hiển thị thông báo lỗi
        echo "Không tìm thấy thông tin sinh viên.";
    }
} else {
    // Nếu không có ID sinh viên được truyền, hiển thị thông báo lỗi
    echo "Không có ID sinh viên được cung cấp.";
}

// Đóng kết nối đến cơ sở dữ liệu
$conn->close();
?>
